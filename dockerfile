FROM node 20.11.1-slim as build

WORKDIR /app

COPY . /app/

RUN npm install
RUN npm run build 

FROM nginx:stable

COPY --from=build /app/build /usr/share/nginx/html

COPY docker/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 

CMD ("nginx", "-g", "daemon off;")

